from setuptools import find_packages
from setuptools import setup

setup(
    name="src",
    packages=find_packages(),
    version="0.0.01",
    description="ML ods",
    author="symclass",
    license="MIT",
)
